This is a port of the following exmples to coldfusion.
https://webservices.amazon.com/paapi5/documentation/without-sdk.html

The project has a docker compose so you can run it with 
`docker-compose up`
This should create a web application environment at http://127.0.0.1:8080 

Replace `<ACCESS_KEY>` and `<SECRET_KEY>` in Application.cfc file with your values.
Optionaly add `PartnerTag` value to payload. 
