component accessors="true" {

    property name="accessKeyID" default = "";
    property name="secretAccessKey" default = "";
    property name="path" default = "";
    property name="regionName" default = "";
    property name="serviceName" default = "";
    property name="httpMethodName" default = "";
    property name="queryParametes" default="";
    property name="awsHeaders" default="";
    property name="payload" default="";

    property name="HMACAlgorithm" default="AWS4-HMAC-SHA256";
    property name="aws4Request" default="aws4_request";
    property name="strSignedHeader" default = "";
    property name="xAmzDate" default = "";
    property name="currentDate" default = "";

    public function init(accessKeyID, secretAccessKey) {
        setaccessKeyID(accessKeyID);
        setsecretAccessKey(secretAccessKey);
        setqueryParametes([]);
        setawsHeaders({});
        setxAmzDate(getTimeStamp());
        setcurrentDate(getDate());
    }


    function addHeader(headerName, headerValue) {
        var awsHeaders = getawsHeaders();
        awsHeaders[headerName] = headerValue;
        setawsHeaders(awsHeaders);
    }


    public function getHeaders() {
        var awsHeaders = getawsHeaders();
        awsHeaders['x-amz-date'] = getxAmzDate();
        var canonicalURL = prepareCanonicalRequest();
        var stringToSign = prepareStringToSign( canonicalURL );
        var signature = calculateSignature( stringToSign );
        awsHeaders['Authorization'] = buildAuthorizationString( signature );
        return awsHeaders;
    }

    private function prepareCanonicalRequest() {
        var lb = chr(10);
        var canonicalURL = "";
        canonicalURL &= gethttpMethodName() & lb;
        canonicalURL &= getpath() & lb & lb;
        var signedHeaders = '';
        var awsHeaders = getawsHeaders();
        var keys = structKeyArray(awsHeaders);
        arraySort(keys, 'text');

        for(var key in keys) {
            signedHeaders &= key & ";";
            canonicalURL &= key & ":" & awsHeaders[key] & lb;
        }

        canonicalURL &= lb;
        setstrSignedHeader(left( signedHeaders, len(signedHeaders) - 1 ));
        canonicalURL &= getstrSignedHeader() & lb;
        canonicalURL &= generateHex( getpayload() );
        return canonicalURL;
    }


    private function calculateSignature(stringToSign) {
        var signatureKey = getSignatureKey ( getsecretAccessKey(), getcurrentDate(), getregionName(), getserviceName() );
        var signature = hmacSha256(stringToSign, signatureKey);
        var strHexSignature = lcase(BinaryEncode( signature, 'hex' ));
        return strHexSignature;
    }

    private function getSignatureKey(key, date, regionName, serviceName) {
        var kSecret = "AWS4" & key;
        kSecret = kSecret.getBytes("UTF-8");
        var kDate = hmacSha256(date, kSecret);
        var kRegion = hmacSha256(regionName, kDate);
        var kService = hmacSha256(serviceName, kRegion);
        var kSigning = hmacSha256(getaws4Request(), kService);
        return kSigning;
    }

    private function prepareStringToSign(canonicalURL) {
        var lb = chr(10);
        var stringToSign = '';
        stringToSign &= getHMACAlgorithm() & lb;
        stringToSign &= getxAmzDate() & lb;
        stringToSign &= getcurrentDate() & "/" & getregionName() & "/" & getserviceName() & "/" & getaws4Request() & lb;
        stringToSign &= generateHex ( canonicalURL );
        return stringToSign;
    }

    private function generateHex(data) {
        return lcase(hash(data, "SHA-256", "UTF-8"));
    }


    private function hmacSha256(data, key, hashType="HmacSHA256") {
        var sformat = "UTF8";
        var ekey    = createObject("java","javax.crypto.spec.SecretKeySpec");
        var secret  = ekey.Init(arguments.key, arguments.hashType);
        var mac     = createObject("java","javax.crypto.Mac");

        //Initialize the MAC
        mac = mac.getInstance(ekey.getAlgorithm());
        mac.init(secret);

        return mac.doFinal(arguments.data.getBytes(sformat));
    }


    private function buildAuthorizationString(strSignature) {
        return getHMACAlgorithm() & " " & "Credential=" & getaccessKeyID() & "/" & getDate() & "/" & getregionName() & "/" & getserviceName() & "/" & getaws4Request() & "," & "SignedHeaders=" & getstrSignedHeader() & "," & "Signature=" & strSignature;
    }

    private function getTimeStamp() {
        var _now = now();
        return dateFormat( _now, 'yyyymmdd' ) & 'T' & timeFormat(_now, 'HHmmss') & 'Z';
    }

    private function getDate() {
        return dateFormat( now(), 'yyyymmdd' );
    } 


}
