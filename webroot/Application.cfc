component {

  function onRequestStart() {

    var host = "webservices.amazon.com";
    var path = "/paapi5/searchitems";
    var payload = '{"PartnerType":"Associates",
        "PartnerTag":"",
        "Keywords":"9780590353427",
        "SearchIndex":"Books",
        "Resources":["Images.Primary.Small","ItemInfo.Title","ItemInfo.ExternalIds","Offers.Listings.Price"]}';
    //Put your Access Key in place of <ACCESS_KEY> and Secret Key in place of <SECRET_KEY> in double quotes
    var awsv4 = new awsV4('<ACCESS_KEY>', '<SECRET_KEY>');
    awsv4.setRegionName("us-east-1");
    awsv4.setServiceName("ProductAdvertisingAPI");
    awsv4.setPath (path);
    awsv4.setPayload (payload);
    awsv4.sethttpMethodName ("POST");
    awsv4.addHeader ('content-encoding', 'amz-1.0');
    awsv4.addHeader ('content-type', 'application/json; charset=utf-8');
    awsv4.addHeader ('host', host);
    awsv4.addHeader ('x-amz-target', 'com.amazon.paapi5.v1.ProductAdvertisingAPIv1.SearchItems');
    var headers = awsv4.getHeaders();
    var headerString = "";

    var httpService = new http(
      method="POST",
      charset="utf-8",
      timeout="300",
      url='https://#host##path#'
    );
    for(var key in headers) {
      httpService.addParam(type="header", name="#key#", value="#headers[key]#");
    }
    httpService.addParam(type="body", value='#payload#');
    var response = httpService.send().getPrefix();
    writeDump(var=deserializeJSON(response.filecontent));
    writeDump(var=response, abort=true);

  }

}